import React, { useState, useEffect } from "react";
import axios from "axios";

const Callapi = () => {
  const [posts, setposts] = useState([]);
  const call = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/posts/")
      .then(response => setposts(response.data))
      .catch(err => {
        console.log(err);
      });
  };
  useEffect(() => call());
  return (
    <div>
      {posts.map(post => (
        <li>{post.title}</li>
      ))}
    </div>
  );
};

export default Callapi;
